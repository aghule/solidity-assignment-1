// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.3;

contract VotingContract{
    
    uint totalVotes = 0;
    uint timeEnd;
    event Message(string);
    event currentVotes(uint);
    
    struct Voter{
        bool verified;
        bool voted;
        uint voteTo;
    }
    
    struct Candidate{
        uint8 id;
        uint count;
    }
    
    Candidate[] public Candidates;
    mapping(address => Voter) voters;
    
    address owner;
    
    constructor(uint timeInMinutes){
        owner = msg.sender;
        
        for(uint8 i = 0 ; i<4;i++)
        {
            Candidates.push(Candidate(i,0));
        }
        
        timeEnd= block.timestamp + timeInMinutes*1 minutes ;
        emit Message("Voting has started");
    }
    
    function verifyVoter(address addr) public{
        require(msg.sender==owner,"Only owner can verify voters");
        require(!(addr==owner),"Creator of contract cannot be verified for voting");
        require(!voters[addr].voted,"Voter already voted");
        voters[addr].verified = true;
    }
    
    function vote(uint8 candId) public{
        require(block.timestamp < timeEnd,"Please vote within the time limit");
        require(!(msg.sender==owner),"Contract owner cannot vote");
        require(candId>=0 && candId<=3, "Not a valid Candidate ID");
        
        Voter storage newVoter = voters[msg.sender];
        require(newVoter.verified,"You are not verified yet");
        require(!newVoter.voted,"You have already voted");
        
        Candidates[candId].count+=1;
        newVoter.voted = true;
        newVoter.voteTo = candId;
        
        totalVotes+=1;
        
        emit currentVotes(totalVotes);
        
    }
    
    function winner() public view returns(uint cid){
        require(block.timestamp >= timeEnd,"Voting process is still going on");
        uint max=0;
        uint id;
        
        for(uint i = 0 ; i<4 ; i++)
        {
            if(Candidates[i].count>max)
            {
                max = Candidates[i].count;
                id = i;
            }
        }
        
        if(max > totalVotes*3/5)
            return id;
        else
            return 404;
    }
    

    function voterDetail(address addr) public view returns(Voter memory voter){
        return voters[addr];
    }
    
    
    fallback() external payable {
	    require(msg.data.length == 0);
	    uint amount = msg.value;
	    payable(msg.sender).transfer(amount);
    }
    
    receive() external payable {
		uint amount = msg.value;
	    payable(msg.sender).transfer(amount);
	}
    
}
